import VueRouter from 'vue-router'

const Home = { template: '<div>Strona Główna</div>' }
const About = { template: '<div>O nas </div>' }
const Contact = { template: '<div>Kontakt</div>' }
const UserGeneral = { template: '<div>Informacje o uzytkowniku<router-view /></div>' }
const User = { props: ['userId'], template: '<div>Id uzytkownika: {{ userId }}</div>' }
const NotFound = { template: '<div>Nie znaleziono strony</div>' }

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home,
            meta: {
                tytul: 'Strona Glowna'
            }
        },
        {
            path: '/onas',
            component: About,
            meta: {
                tytul: 'O nas'
            }
        },
        {
            path: '/kontakt',
            alias: '/contact',
            component: Contact
        },
        {
            path: 'uzytkownicy',
            component: UserGeneral,
            children: [
                {
                    path: 'profil/:userId',
                    component: User,
                    props: true
                }
            ]
        },
        {
            path: '/uzytkownik/:userId',
            component: User,
            props: true
        },
        {
            path: '*',
            component: NotFound
        }
    ]
});

router.afterEach((to) => {
    document.title = to.meta.tytul;
});

export default router;