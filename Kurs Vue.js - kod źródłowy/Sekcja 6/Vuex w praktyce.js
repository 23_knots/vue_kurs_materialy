import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        licznik: 0
    },
    mutations: {
        dodaj(state) {
            state.licznik++;
        },
        odejmij(state) {
            state.licznik--
        }
    },
    action: {
        zwiekszLicznik ({ commit }) {
            commit('dodaj');
        },
        zmienjszLicznik ({ commit }) {
            commit('odejmij');
        }
    },
    getters: {
        pobierzLicznik: (state) => {
            return state.licznik;
        }
    }
})